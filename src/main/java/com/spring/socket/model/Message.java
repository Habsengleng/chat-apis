package com.spring.socket.model;

import javax.persistence.*;

@Entity
public class Message {
    @Id
    @GeneratedValue
    private int id;

    @ManyToOne(cascade= CascadeType.ALL)
    private User user;
    private String message;

    public Message(int id, User user, String message) {
        this.id = id;
        this.user = user;
        this.message = message;
    }

    public Message() {
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "Message [user=" + user + ", message=" + message + "]";
    }
}
