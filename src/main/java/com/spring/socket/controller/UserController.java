package com.spring.socket.controller;

import com.spring.socket.model.User;
import com.spring.socket.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class UserController {
    @Autowired
    private UserRepository userRepository;

    @GetMapping("api/users")
    public List<User> findAll() {
        return userRepository.findAll();
    }
}
