package com.spring.socket.controller;

import com.spring.socket.model.Message;
import com.spring.socket.repository.MessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class MessageRestController {

	@Autowired
	private MessageRepository messageRepository;
	
	@GetMapping("/api/messages")
	public List<Message> findAll() {
		return messageRepository.findAll();
	}
	
}
